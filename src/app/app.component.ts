import { Component, OnInit } from '@angular/core';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

declare let tableau: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'app';

  ngOnInit(){
    this.intiViz();
  }

  intiViz(){

    const containerDiv = document.getElementById('tableauDiv');
    const url = 'https://public.tableau.com/views/Killedbyabear/Killedbybears?:embed=y&:embed_code_version=3&:loadOrderID=0&:display_count=yes&:origin=viz_share_link';

    let viz = new tableau.Viz(containerDiv, url);

  }

}
